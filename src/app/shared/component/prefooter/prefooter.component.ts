import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prefooter',
  templateUrl: './prefooter.component.html',
  styleUrls: ['./prefooter.component.scss']
})
export class PrefooterComponent implements OnInit {

  line01: any = [
    {
      cidade: 'PARAÍBA',
      atendimento: 'FIORI - JOÃO PESSOA',
      fone: 'Fone: (83) 3142-0954'
    },
    {
      cidade: 'RIO GRANDE DO NORTE',
      atendimento: 'TOYOLEX NATAL',
      fone: 'Fone: (84) 4042-0503'
    }
  ];


  constructor() { }

  ngOnInit() {
  }
}
