import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgxCaptchaModule } from 'ngx-captcha';
import { Ng5SliderModule } from 'ng5-slider';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HeaderComponent } from './shared/component/header/header.component';
import { FooterComponent } from './shared/component/footer/footer.component';
import { OfertasComponent } from './shared/component/ofertas/ofertas.component';
import { HomeComponent } from './layout/home/home.component';
import { DetalhesComponent } from './layout/detalhes/detalhes.component';
import { PropostaComponent } from './shared/component/proposta/proposta.component';
import { BannerComponent } from './shared/component/banner/banner.component';
import { SearchComponent } from './shared/component/search/search.component';
import { DicasComponent } from './shared/component/dicas/dicas.component';
import { NewsletterComponent } from './shared/component/newsletter/newsletter.component';
import { ImagenComponent } from './shared/component/imagen/imagen.component';
import { SimulaComponent } from './shared/component/simula/simula.component';
import { CarouselComponent } from './shared/component/carousel/carousel.component';
import { OpcionComponent } from './shared/component/opcion/opcion.component';
import { EstoqueComponent } from './layout/estoque/estoque.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoopbackService } from './shared/service/loopback.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { FaleConoscoComponent } from './layout/fale-conosco/fale-conosco.component';
import { RdStationService } from './shared/service/rd-station.service';
import { PrefooterComponent } from './shared/component/prefooter/prefooter.component';
import { BottomComponent } from './shared/component/bottom/bottom.component';
import { StockComponent } from './shared/component/stock/stock.component';
import { CurrencyBrasilPipe } from './shared/pipe/currency-brasil.pipe';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    OfertasComponent,
    HomeComponent,
    DetalhesComponent,
    PropostaComponent,
    BannerComponent,
    SearchComponent,
    DicasComponent,
    NewsletterComponent,
    ImagenComponent,
    SimulaComponent,
    CarouselComponent,
    OpcionComponent,
    EstoqueComponent,
    FaleConoscoComponent,
    PrefooterComponent,
    BottomComponent,
    StockComponent,
    CurrencyBrasilPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    HttpClientModule,
    NgxCaptchaModule,
    ReactiveFormsModule,
    FormsModule,
    NgxSpinnerModule,
    Ng5SliderModule,
    LazyLoadImageModule
  ],
  providers: [LoopbackService, RdStationService],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
