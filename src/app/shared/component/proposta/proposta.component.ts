import { Component, OnInit, Input } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-proposta',
  templateUrl: './proposta.component.html',
  styleUrls: ['./proposta.component.scss']
})
export class PropostaComponent implements OnInit {

  @Input() carro: number;
  public vei: any;
  public valor: any;
  fmProposta: FormGroup;
  public sitekey = '6Lcyj5IUAAAAAC5TIimT0cPm6dGTHYMv4Sk7si0v';
  constructor(
    private lb: LoopbackService,
    private fb: FormBuilder
  ) {
    this.onCarro(this.carro);
  }

  ngOnInit() {
    this.buildForm();
  }

  onCarro(id) {
    this.lb.getFindBy('carros', 'id', id)
    .subscribe(data => {
      this.valor = data[0].valor;
      this.vei = `${data[0].marcaCarro} ${data[0].modeloCarro}`;
    });
  }

  buildForm() {
    this.fmProposta = this.fb.group({
      nome: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      rg: [''],
      cpf: ['', Validators.required],
      telefone: ['', Validators.required],
      celular: [''],
      veiculo: [''],
      valor: [''],
      recaptcha: ['']
    });
  }

  // onSubmit() {
  //   if (!this.fmProposta.valid) {
  //     return;
  //   }
  //   this.lb.getFindBy('carros', 'id', id)
  //   .subscribe(data => {
  //     this.valor = data[0].valor;
  //     this.vei = `${data[0].marcaCarro} ${data[0].modeloCarro}`;
      
  //   });
  //   console.log(this.fmProposta.value);
  // }
}
