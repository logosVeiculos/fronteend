import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RdStationService } from 'src/app/shared/service/rd-station.service';
import Swal from 'sweetalert2';
import { ReturnStatement } from '@angular/compiler';
import { NgxSpinnerComponent, NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-fale-conosco',
  templateUrl: './fale-conosco.component.html',
  styleUrls: ['./fale-conosco.component.scss']
})
export class FaleConoscoComponent implements OnInit {

  fmConoco: FormGroup;

  constructor(
    private fb: FormBuilder,
    private rd: RdStationService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
        this.spinner.hide();
    }, 1000);
    this.buildForm();
  }

  buildForm() {
    this.fmConoco = this.fb.group({
      nome: ['', Validators.required],
      telefone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mensagem: ['', Validators.required]
    });
  }

  onSend() {
    if (!this.fmConoco.valid) {
      return;
    }
    this.rd.sendCotact(this.fmConoco.value)
    .subscribe(data => {
      console.log(data);
      Swal.fire(
        'Mensagem enviado',
        'Em um momento entraremos em contato com você',
        'success'
      );
    });
  }
}
