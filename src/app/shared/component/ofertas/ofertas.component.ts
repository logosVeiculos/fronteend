import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';

@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.component.html',
  styleUrls: ['./ofertas.component.scss']
})
export class OfertasComponent implements OnInit {

  @Input() oferta: number;
  @Input() limit: number;
  carros: any;


  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    this.onOfertas(this.oferta, this.limit);
  }

  onOfertas(field, limit) {
    this.lb.getOfertas(field, limit).subscribe(data => {
      this.carros = data;
      console.log('Ofertas = ', field);
      console.log('Limits = ', limit);
      console.log('data = ', data);
    }, error => {
      console.log(error);
    });
  }

  public round(value, precision) {
    const multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }

}
