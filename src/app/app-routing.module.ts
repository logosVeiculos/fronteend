import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './layout/home/home.component';
import { DetalhesComponent } from './layout/detalhes/detalhes.component';
import { EstoqueComponent } from './layout/estoque/estoque.component';
import { FaleConoscoComponent } from './layout/fale-conosco/fale-conosco.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'estoque', component: EstoqueComponent},
  {path: 'veiculo/:id', component: DetalhesComponent},
  {path: 'fale-conosco', component: FaleConoscoComponent},
  {path: '**', redirectTo: '/home'}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
