import { Component, OnInit, Input } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {
  @Input() limit: number;
  carros: any;


  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    this.onOfertas(this.limit);
  }

  onOfertas(limit) {
    this.lb.getFindBy('carros', 'status', 'disponivel', limit, 'DESC').subscribe(data => {
      this.carros = data;
    });
  }

  public round(value, precision) {
    const multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }

}