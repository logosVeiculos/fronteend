import { Component, OnInit } from '@angular/core';
import { LoopbackService } from 'src/app/shared/service/loopback.service';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-estoque',
  templateUrl: './estoque.component.html',
  styleUrls: ['./estoque.component.scss']
})
export class EstoqueComponent implements OnInit {

  carros: any;
  val: any;
  sql: string;
  marcas;
  marca;
  transmissao;
  portas;
  combustivel;
  ano;

  constructor(
    private lb: LoopbackService,
    private spinner: NgxSpinnerService
  ) {
    this.lb.data.subscribe(u => {
      this.val = u;
    });
  }

  ngOnInit() {
    this.getMarca();
    if (!this.val) {
      this.lb.getFindBy('carros', 'status', 'disponivel').subscribe(data => this.carros = data);
    } else {
      console.log('Home');
      this.lb.getSearch(this.val.marca, this.val.de, this.val.ate, this.val.modelo).subscribe(data => {
        this.carros = data;
      }, err => {
        this.carros = 'Sin produto';
      });
    }
    this.spinner.show();
    setTimeout(() => {
        this.spinner.hide();
    }, 1000);
  }

  onSearch() {
    this.lb.getSearch(this.val.marca, this.val.de, this.val.ate, this.val.modelo)
    .subscribe(data => this.carros = data);
  }

  getOfertas() {
    this.lb.getEstoque(this.marca, this.transmissao, this.combustivel, this.portas, this.ano).subscribe(data => {
      // tslint:disable-next-line:no-string-literal
      if (data['length']) {
        this.carros = data;
      } else {
        Swal.fire({
          type: 'error',
          title: 'WTF!',
          text: 'Não têm carro que mostrar!'
        }).then(() => this.getCarros());
      }
    });
  }

  getCarros() {
    this.lb.getFindBy('carros', 'status', 'disponivel').subscribe(data => this.carros = data);
  }

  public round(value, precision) {
    const multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }

  getMarca() {
    this.lb.getTable('marcas')
    .subscribe(data => { this.marcas = data; });
  }
}
